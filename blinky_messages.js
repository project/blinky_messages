(function ($) {

Drupal.behaviors.blinkyMessages = {
  attach: function (context) {
    $('.messages:visible').once('blinky-messages', function() {
      for (i=0; i < 6; i++) {
        // For some reason I can't get the jQuery UI effect to actually work.
        //$(this).effect('pulsate');
        $(this).fadeTo(300, 0).fadeTo(300, 1);
      }
    });
  }
};

})(jQuery);
